const mongoose = require('mongoose');
const dotenv = require('dotenv').config();

var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');


var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


mongoose.set('useFindAndModify', false);
mongoose.connect(process.env.connectionURI, {useNewUrlParser: true}).then(() => {
    console.log("Connection Successful!");
}, (error) => {
    console.log("Something went wrong!! ", error);
});


var indexRouter = require('./routes/index');
app.use('/', indexRouter);
var usersRouter = require('./routes/users.route.js');
app.use( '/users', usersRouter);


module.exports = app;
