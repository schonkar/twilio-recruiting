mongoose = require('mongoose');

usersSchema = new mongoose.Schema({
  firstName: {
    type: String
  },
  lastName: {
    type: String
  },
  email: {
    type: String
  },
  phone: {
    type: String
  },
  time: {
    type: String
  }
}, {timestamps: true});

module.exports = mongoose.model('User', usersSchema);
