router = require('express').Router();
User = require('../models/users.model');

const Twilio = require('twilio');
const twilio_sid = "AC6ec97d5a3963fdd5612e66d82eef5e76";
const twilio_token = "7afe11810af3a5890d980abf7a77c5f7";
const client = new Twilio(twilio_sid, twilio_token);

//(970) 300-5220

router.get('/', function(req, res, next) {
  User.find().then(response => {
    if (response.length > 0)
      res.status(200).send(response);
    else
      res.status(204).json({message: "Not found!"});
    }
  ).catch((error) => {
    res.status(500).send(error);
  });
});

router.post('/', function(req, res, next) {
  let to = req.body.To;
  let from = req.body.From;
  let body = req.body.Body;

  if (body) {
    console.log("Body", body);
    User.findOne({phone: req.body.From}).then(response => {
      // console.log("response", response);
      if (!response) {
        let user = new User({phone: from});
        user.save().then(response => {
          console.log(response);
          client.messages.create({to: response.phone, from: to, body: "\n Hi to our recruitment portal! Please provide your first name: "})
          // res.send(response);
        }).catch((error) => {
          res.status(500).send(error);
        });
        res.end();
      } else if (response && !response.firstName && !response.lastName && !response.email && !response.time) {
        let user = new User({firstName: body});
        User.findOneAndUpdate({
          phone: from
        }, {
          firstName: body
        }, (response) => {
          console.log(response);
          client.messages.create({to: from, from: to, body: "\n Please provide your last name: "})
          // res.send(response);
        })
        res.end();
      } else if (response && !response.lastName && !response.email && !response.time) {
        let user = new User({lastName: body});
        User.findOneAndUpdate({
          phone: from
        }, {
          lastName: body
        }, (response) => {
          console.log(response);
          client.messages.create({to: from, from: to, body: "\n Please provide your Email: "})
          // res.send(response);
        })
        res.end();
      } else if (response && !response.email && !response.time) {
        let user = new User({email: body});
        User.findOneAndUpdate({
          phone: from
        }, {
          email: body
        }, (response) => {
          console.log(response);
          client.messages.create({to: from, from: to, body: "\n Please provide the best time to talk to you "})
          // res.send(response);
        })
        res.end();
      } else if (response && !response.time) {
        let user = new User({time: body});
        User.findOneAndUpdate({
          phone: from
        }, {
          time: body
        }, (response) => {
          console.log(response);
          client.messages.create({to: from, from: to, body: "\n Thank you! Some one from our Office will be contacting you soon!"})
          // res.send(response);
        })
        res.end();
      } else if (response && response.firstName && response.lastName && response.email && response.time) {
        client.messages.create({to: from, from: to, body: "\n It seems like we already have your records. Please be patient while someone reaches out to you. Thank you!"})
        res.end();
      }
    })
  }
});

module.exports = router;
